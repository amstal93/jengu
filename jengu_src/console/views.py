from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.template import loader
from django.views import View
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.core.exceptions import PermissionDenied
from django.contrib import messages
from django.conf import settings
from django.db.models import F

from .models import Client, Consultation
from .forms import AddClientForm, RecordConsultationForm, UpdateConsultationForm


def logout_view(request):
    logout(request)
    return redirect('{}?next={}'.format(settings.LOGIN_URL, "/console/"))


@method_decorator(login_required, name='dispatch')
class Home(TemplateView):

    template_name = "console/index.html"


@method_decorator(login_required, name='dispatch')
class BrowseAndCreate(TemplateView,CreateView ):
    """display a bunch of items from a model and the form to create new instances"""

    def get_context_data(self, **kwargs):
        context = TemplateView.get_context_data(self, **kwargs)
        context['form'] = self.form_class
        context["status"] = self.request.GET.get('status', self.default_status)
        context["order"] = self.request.GET.get('order_by', self.default_order)
        context['item_list'] = type(self).model.objects.filter(
            fk_owner=self.request.user.id, status=context["status"]).order_by(F("{}".format(context["order"])).desc(nulls_last=True))

        return context

    def post(self, request):
        super().post(request)
        return redirect(self.success_url)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.fk_owner = self.request.user
        self.object.status = self.get_context_data()["status"]
        super().form_valid(form)
        messages.add_message(self.request, messages.SUCCESS,
                             "Recorded in {}".format(self.object.status))

    def form_invalid(self, form):
        for e in form.errors.items():
            messages.add_message(self.request, messages.WARNING,
                                 "Invalid {}: {}".format(e[0], e[1][0]))


@method_decorator(login_required, name='dispatch')
class BrowseClients(BrowseAndCreate):
    template_name = "console/browse_clients.html"
    model = Client
    success_url = 'browse_clients'
    form_class = AddClientForm
    default_order = "inscription"
    default_status = "active"


@method_decorator(login_required, name='dispatch')
class BrowseConsultations(BrowseAndCreate):
    template_name = "console/browse_consultations.html"
    model = Consultation
    form_class = RecordConsultationForm
    success_url = 'browse_consultations'

    default_order = "date"
    default_status = "booked"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['item_list'] = type(self).model.objects.prefetch_related('fk_client').filter(
            fk_owner=self.request.user.id, status=context["status"]).order_by(F(context["order"])
            .desc(nulls_first=True)).select_related('fk_type')
        context['form'] = self.form_class(user = self.request.user)
        context['form_consultation'] = ConsultationEdit().get_form_class()()
        return context

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

@method_decorator(login_required, name='dispatch')
class ClientDetails(TemplateView):
    template_name = "console/details.html"
    success_url = 'client_details'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["client"] = Client.objects.get(pk=self.kwargs['pk'])
        context["consultations"] = Consultation.objects.filter(fk_client=self.kwargs['pk'])
        context["form"] = ClientEdit().get_form_class()(initial = context["client"].__dict__)
        return context

@method_decorator(login_required, name='dispatch')
class ClientEdit(UpdateView):
    model = Client
    form_class = AddClientForm
    template_name_suffix = '_update_form'

    def get_object(self, *args, **kwargs):
        obj = super(ClientEdit, self).get_object(*args, **kwargs)
        if obj.fk_owner != self.request.user:
            raise PermissionDenied() 
        return obj

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS,
                             "Successfully updated")
        self.success_url = self.request.META.get('HTTP_REFERER', '/console/clients')
        return super().form_valid(form)

    def form_invalid(self, form):
        for e in form.errors.items():
            messages.add_message(self.request, messages.WARNING,
                                 "Invalid {}: {}".format(e[0], e[1][0]))

        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER', '/console/clients'))
        return super().form_invalid(form)

@method_decorator(login_required, name='dispatch')
class ConsultationEdit(UpdateView):
    model = Consultation
    form_class = UpdateConsultationForm
    template_name_suffix = '_update_form'

    def form_invalid(self, form):
        for e in form.errors.items():
            messages.add_message(self.request, messages.WARNING,
                                 "Invalid {}: {}".format(e[0], e[1][0]))
        return HttpResponseRedirect(self.request.META.get('HTTP_REFERER', '/console/clients'))

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS,
                             "Consultation {} successfully updated".format(self.kwargs['pk']))
        self.success_url = self.request.META.get('HTTP_REFERER', '/console/clients')
        return super().form_valid(form)
