from django import forms
from datetime import date
from .models import Client, Consultation
from .validators import is_base64
from django.conf import settings
import datetime as dt


class RecordConsultationForm(forms.ModelForm):
    class Meta:
        model = Consultation
        fields = ["fk_type", "fk_client", "duration", "date"]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        self.fields["date"] = forms.DateTimeField(input_formats=settings.DATE_TIME_INPUT_FORMATS)
        self.fields['fk_client'].queryset = Client.objects.filter(fk_owner=user)
        self.fields['duration'] = forms.ChoiceField(choices = zip(range(0,10800,60),["H: {} m: {}".format(i//60, i%60) for i in range(180)]))

class UpdateConsultationForm(forms.ModelForm):
    class Meta:
        model = Consultation
        fields = ["fk_type", "duration", "date", "payed", "status"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["date"] = forms.DateTimeField(input_formats=settings.DATE_TIME_INPUT_FORMATS)

class AddClientForm(forms.ModelForm):
    class Meta:
        model = Client
        fields = ["first_name", "last_name", "birth_date", "phone",
                  "mail", "notes", "is_legal", "legal_representative"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["first_name"] = forms.CharField(
            widget=forms.TextInput(attrs={'class': 'input is-primary 2encrypt'}))
        self.fields["last_name"] = forms.CharField(
            widget=forms.TextInput(attrs={'class': 'input is-primary 2encrypt'}))
        self.fields["birth_date"] = forms.DateField(widget=forms.DateInput(format=settings.DATE_FORMAT, attrs={'placeholder': '31/03/1989', 'class': 'input is-primary'}),
                                                    input_formats=settings.DATE_INPUT_FORMATS)
        self.fields["phone"] = forms.CharField(
            required=False, widget=forms.TextInput(attrs={'class': 'input is-info 2encrypt'}))
        self.fields["notes"] = forms.CharField(
            widget=forms.Textarea(attrs={'class': 'textarea 2encrypt'}), required=False)
        self.fields["mail"] = forms.CharField(required=False, widget=forms.EmailInput(
            attrs={'class': 'input is-info 2encrypt', 'type': 'email'}))
        self.fields["is_legal"] = forms.BooleanField(
            required=False, initial=True, widget=forms.CheckboxInput(attrs={'class': 'checkbox'}), help_text="Is he (she) of lawful age and legally competent? If not, Indicate a legal representative")
        self.fields["legal_representative"] = forms.CharField(
            required=False, widget=forms.TextInput(attrs={'class': 'input 2encrypt'}))
